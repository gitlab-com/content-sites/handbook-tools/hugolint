package hugo_test

import (
	"errors"
	"testing"
	"testing/fstest"

	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/hugo"
)

func TestPage_AbsLink(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name       string
		path       string
		webRootDir string
		want       string
		wantErr    error
	}{
		{
			name:       "main page",
			path:       "test/content/_index.md",
			webRootDir: "test/content",
			want:       "/",
		},
		{
			name:       "sub page",
			path:       "test/content/sub/_index.md",
			webRootDir: "test/content",
			want:       "/sub/",
		},
		{
			name:       "non-index page",
			path:       "test/content/sub/foo.md",
			webRootDir: "test/content",
			want:       "/sub/foo/",
		},
		{
			name:       "outside of webroot",
			path:       "test/layout/tmpl.md",
			webRootDir: "test/content",
			wantErr:    hugo.ErrOutsideWebRoot,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			fs := fstest.MapFS{
				tc.path: &fstest.MapFile{},
			}

			p, err := hugo.LoadPage(tc.path, hugo.WithFS(fs))
			if err != nil {
				t.Fatalf("LoadPage(%q): %v", tc.path, err)
			}

			got, err := p.AbsLink(tc.webRootDir)
			if !errors.Is(err, tc.wantErr) {
				t.Fatalf("AbsLink(%q): %v, want %v", tc.webRootDir, err, tc.wantErr)
			}

			if err != nil {
				return
			}

			if got != tc.want {
				t.Errorf("AbsLink() = %q, want %q", got, tc.want)
			}
		})
	}
}
