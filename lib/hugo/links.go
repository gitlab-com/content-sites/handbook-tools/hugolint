package hugo

import (
	"errors"
	"fmt"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"unicode"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/text"
)

type Link struct {
	Page       *Page
	Filename   string
	Href       string
	RelativeTo string
	Line       int
}

var ErrExternal = errors.New("external link")

// Abs returns the absolute URL of a link.
// If Link is an external link, it returns an the ErrExternal error.
func (l Link) Abs(webRootDir string) (string, error) {
	u, err := url.Parse(l.Href)
	if err != nil {
		return "", l.anchorError(err)
	}

	if u.Scheme != "" || u.Host != "" {
		return "", fmt.Errorf("%w: %q is a remote URL", ErrExternal, l.Href)
	}

	p := u.Path

	switch {
	case path.IsAbs(u.Path):
		return u.String(), nil

	case p == "":
		var err error

		u.Path, err = l.Page.AbsLink(webRootDir)
		if err != nil {
			return "", err
		}

		return u.String(), nil

	default:
		absLink, err := l.Page.AbsLink(webRootDir)
		if err != nil {
			return "", err
		}

		u.Path = path.Join(absLink, u.Path)

		// path.Join swallows trailing slashes. Add them back.
		if strings.HasSuffix(p, "/") && !strings.HasSuffix(u.Path, "/") {
			u.Path += "/"
		}

		return u.String(), nil
	}
}

type Heading struct {
	Filename string
	Slug     string
	Line     int
}

func (p *Page) Links() ([]Link, error) {
	var (
		parser = goldmark.DefaultParser()
		root   = parser.Parse(text.NewReader(p.data))
	)

	return p.links(root)
}

// Headings returns a list of headings in the page p.
func (p *Page) Headings() ([]Heading, error) {
	prs := goldmark.DefaultParser()
	prs.AddOptions(
		parser.WithAutoHeadingID(),
		parser.WithHeadingAttribute(),
	)

	pctx := parser.NewContext(
		parser.WithIDs(&GithubAutoID{}),
	)

	root := prs.Parse(text.NewReader(p.data), parser.WithContext(pctx))

	return p.headings(root)
}

func (p *Page) newHeading(heading *ast.Heading) (*Heading, error) {
	lineNo, err := p.PosToLineNo(positionOf(heading))
	if err != nil {
		return nil, err
	}

	value, err := p.Line(lineNo)
	if err != nil {
		return nil, err
	}

	slug, err := slug(heading)
	if err != nil {
		return nil, fmt.Errorf("heading %q: %w", value, err)
	}

	result := &Heading{
		Filename: p.path,
		Slug:     slug,
		Line:     lineNo,
	}

	return result, nil
}

func slug(heading *ast.Heading) (string, error) {
	rawID, ok := heading.AttributeString("id")
	if !ok {
		// Attribute "id" does not exist.
		// If that happens, we're probably missing the WithAutoHeadingID() option.
		//nolint:err113
		return "", errors.New(`the "id" attribute is missing`)
	}

	id, ok := rawID.([]byte)
	if !ok {
		// Attribute "id" exists, but it's not a []byte.
		// This shouldn't happen.
		//nolint:err113
		return "", fmt.Errorf("unexpected type: got %T, want []byte", rawID)
	}

	return string(id), nil
}

func (p *Page) headings(node ast.Node) ([]Heading, error) {
	/*
		This currently reads a file directly from disk,
		so if the target heading is contained in a file that is included from
		another file, it will be missed. This is a piece of future work to fix.
	*/
	var ret []Heading

	if h, ok := node.(*ast.Heading); ok {
		heading, err := p.newHeading(h)
		if err != nil {
			return nil, fmt.Errorf("newHeading: %w", err)
		}

		ret = append(ret, *heading)
	}

	for child := node.FirstChild(); child != nil; child = child.NextSibling() {
		heading, err := p.headings(child)
		if err != nil {
			// not annotating recursive call
			return nil, err
		}

		ret = append(ret, heading...)
	}

	return ret, nil
}

func (p *Page) links(node ast.Node) ([]Link, error) {
	var ret []Link

	if link, ok := node.(*ast.Link); ok {
		line, err := p.PosToLineNo(positionOf(link))
		if err != nil {
			return nil, err
		}

		ret = append(ret, Link{
			Page:       p,
			Filename:   p.path,
			Href:       string(link.Destination),
			RelativeTo: path.Dir(p.path),
			Line:       line,
		})
	}

	for child := node.FirstChild(); child != nil; child = child.NextSibling() {
		links, err := p.links(child)
		if err != nil {
			return nil, err
		}

		ret = append(ret, links...)
	}

	return ret, nil
}

func positionOf(node ast.Node) int {
	switch node.Type() {
	case ast.TypeBlock:
		return node.Lines().At(0).Start
	case ast.TypeInline:
		return positionOf(node.Parent())
	case ast.TypeDocument:
		break
	}

	return -1
}

type brokenLinksOptions struct {
	checkAnchors bool
}

type BrokenLinksOption func(*brokenLinksOptions)

// WithAnchors controls whether BrokenLinks also checks the validity of anchors, i.e. headings within the files.
func WithAnchors(check bool) BrokenLinksOption {
	return func(o *brokenLinksOptions) {
		o.checkAnchors = check
	}
}

func (p *Page) BrokenLinks(webRootDir string, options ...BrokenLinksOption) ([]Link, error) {
	var opts brokenLinksOptions

	for _, option := range options {
		option(&opts)
	}

	allLinks, err := p.Links()
	if err != nil {
		return nil, err
	}

	return p.filterBroken(webRootDir, allLinks, opts.checkAnchors)
}

func (p *Page) filterBroken(webRootDir string, allLinks []Link, checkAnchors bool) ([]Link, error) {
	var brokenLinks []Link

	for _, link := range allLinks {
		isBroken, err := p.isBroken(webRootDir, link, checkAnchors)
		if err != nil {
			return nil, err
		}

		if isBroken {
			brokenLinks = append(brokenLinks, link)
		}
	}

	return brokenLinks, nil
}

func (p *Page) isFragmentValid(path string, fragment string) (bool, error) {
	fragment = strings.TrimSpace(fragment)
	fragment = trimTextFragment(fragment)

	if fragment == "" {
		return true, nil
	}

	page, err := LoadPage(path, WithFS(p.fs))
	if err != nil {
		return false, err
	}

	headings, err := page.Headings()
	if err != nil {
		return false, err
	}

	for _, heading := range headings {
		// Check for slug equality
		if heading.Slug == fragment {
			return true, nil
		}
	}

	return false, nil
}

// trimTextFragments removes any text URL fragments, i.e. URL fragments that reference specific text on a page.
// These text fragments have the form `anchor:~:text=Text+to+highlight`, which would return `anchor`.
func trimTextFragment(s string) string {
	i := strings.Index(s, ":~:")
	if i < 0 {
		return s
	}

	return s[:i]
}

func (p *Page) isBroken(webRootDir string, link Link, withAnchors bool) (bool, error) {
	u, err := url.Parse(link.Href)
	if err != nil {
		return false, link.anchorError(err)
	}

	// ignore remote URLs
	if u.Scheme != "" || u.Host != "" {
		return false, nil
	}

	path := u.Path

	switch {
	case filepath.IsAbs(path):
		path = filepath.Join(webRootDir, strings.TrimPrefix(path, "/"))
	case path == "":
		path = p.path
	default:
		path = filepath.Join(link.RelativeTo, path)
	}

	path = strings.TrimSuffix(path, "index.html")
	path = strings.TrimSuffix(path, ".html")
	path = strings.TrimSuffix(path, "/")

	if p.haveIndexFile(path) { // nolint:nestif
		var indexFile string
		indexFile, err = p.getIndexFile(path)

		if err != nil {
			return true, err
		}

		path = path + "/" + indexFile
	} else {
		mdFile := path
		if !strings.HasSuffix(mdFile, ".md") {
			mdFile += ".md"
		}

		if !p.IsRegular(mdFile) {
			return true, nil
		}

		path = mdFile
	}

	if !withAnchors {
		return false, nil
	}

	fragmentValid, err := p.isFragmentValid(path, u.Fragment)
	if err != nil {
		return true, err
	}

	return !fragmentValid, nil
}

func (p *Page) getIndexFile(dir string) (string, error) {
	indexFiles := []string{
		"_index.md",
		"index.md",
		"_index.html",
	}

	if !p.IsDir(dir) {
		return "", fmt.Errorf("%w: index file for %s", os.ErrNotExist, p.path) //nolint:err113
	}

	for _, indexFile := range indexFiles {
		if p.IsRegular(filepath.Join(dir, indexFile)) {
			return indexFile, nil
		}
	}

	return "", fmt.Errorf("%w: index file for %s", os.ErrNotExist, p.path) //nolint:err113
}

func (p *Page) haveIndexFile(dir string) bool {
	indexFiles := []string{
		"_index.md",
		"index.md",
		"_index.html",
	}

	if !p.IsDir(dir) {
		return false
	}

	for _, indexFile := range indexFiles {
		if p.IsRegular(filepath.Join(dir, indexFile)) {
			return true
		}
	}

	return false
}

func (l Link) anchorError(err error) error {
	return AnchoredError{
		Link: l,
		Err:  err,
	}
}

type AnchoredError struct {
	Link
	Err error
}

func (e AnchoredError) Error() string {
	return fmt.Sprintf("[%s:%d] %v", e.Filename, e.Line, e.Err)
}

func (e AnchoredError) Unwrap() error {
	return e.Err
}

// GithubAutoID implements the goldmark/parser.IDs interface.
//
// It generates slugs for headings, similar to how GitHub does it.
// It also ensures that the generated slugs are unique.
type GithubAutoID struct {
	ids map[string]bool
}

func (gh *GithubAutoID) Generate(value []byte, kind ast.NodeKind) []byte {
	var (
		str = strings.TrimSpace(string(value))
		sb  strings.Builder
	)

	for _, r := range str {
		switch {
		case r == ' ':
			sb.WriteRune('-')
		case r == '-' || r == '_':
			sb.WriteRune(r)
		case unicode.IsLetter(r) || unicode.IsNumber(r):
			sb.WriteRune(unicode.ToLower(r))
		}
	}

	if sb.Len() == 0 {
		switch kind {
		case ast.KindHeading:
			sb.WriteString("heading")
		default:
			sb.WriteString("id")
		}
	}

	// If necessary, append a hyphen and a number to make the ID unique.
	var (
		count  int
		suffix string
	)

	for gh.ids[sb.String()+suffix] {
		count++
		suffix = fmt.Sprintf("-%d", count)
	}

	sb.WriteString(suffix)

	id := []byte(sb.String())
	gh.Put(id)

	return id
}

func (gh *GithubAutoID) Put(id []byte) {
	if gh.ids == nil {
		gh.ids = make(map[string]bool)
	}

	gh.ids[string(id)] = true
}
