package hugo

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"slices"
	"strings"
)

type Page struct {
	path   string
	data   []byte
	toLine []int

	fs fs.StatFS
}

var DefaultFilesystem fs.StatFS = systemFS{}

func LoadPage(path string, opts ...PageOption) (*Page, error) {
	pageOptions := pageOptions{
		fs: systemFS{},
	}

	for _, o := range opts {
		o(&pageOptions)
	}

	fh, err := pageOptions.fs.Open(path)
	if err != nil {
		return nil, fmt.Errorf("fs.Open(%q): %w", path, err)
	}
	defer fh.Close()

	data, err := io.ReadAll(fh)
	if err != nil {
		return nil, fmt.Errorf("io.ReadAll: %w", err)
	}

	return &Page{
		path: path,
		data: data,
		fs:   pageOptions.fs,
	}, nil
}

var ErrOutsideWebRoot = errors.New("page is outside of webroot")

// AbsLink returns the absolute link to the page.
func (p *Page) AbsLink(webRootDir string) (string, error) {
	relPath, err := filepath.Rel(webRootDir, p.path)
	if err != nil {
		return "", fmt.Errorf("filepath.Rel(%q, %q): %w", webRootDir, p.path, err)
	}

	relPath = "/" + filepath.ToSlash(relPath)

	if strings.HasPrefix(relPath, "/../") {
		return "", fmt.Errorf("%w: page.path=%q, webRootDir=%q", ErrOutsideWebRoot, p.path, webRootDir)
	}

	suffixes := []string{
		"/_index.md",
		"/_index.html",
		"/index.md",
		"/index.html",
		".md",
		".html",
	}
	for _, suffix := range suffixes {
		if strings.HasSuffix(relPath, suffix) {
			relPath = strings.TrimSuffix(relPath, suffix) + "/"
			break
		}
	}

	return relPath, nil
}

type pageOptions struct {
	fs fs.StatFS
}

type PageOption func(o *pageOptions)

func WithFS(fs fs.StatFS) PageOption {
	return func(o *pageOptions) {
		o.fs = fs
	}
}

// PosToLineNo returns the line number of the byte at position pos.
// PosToLineNo numbers are 1-indexed, i.e. the first line in a file is returned as 1, not 0.
func (p *Page) PosToLineNo(pos int) (int, error) {
	if pos < 0 || pos >= len(p.data) {
		return 0, fmt.Errorf("position out of range: %d", pos) // nolint:err113
	}

	p.initToLine()

	index, _ := slices.BinarySearch(p.toLine, pos)

	return index + 1, nil
}

// Line returns the requested line as a string.
// Line numbers are 1-indexed, i.e. the smallest valid value is 1, not 0.
func (p *Page) Line(lineNo int) (string, error) {
	index := lineNo - 1

	if index < 0 || index >= len(p.toLine) {
		// nolint:err113
		return "", fmt.Errorf("line %d is out of bounds: index = %d, len(p.toLine) = %d", lineNo, index, len(p.toLine))
	}

	if index == 0 {
		return string(p.data[0:p.toLine[index]]), nil
	} else {
		return string(p.data[p.toLine[index-1]+1 : p.toLine[index]]), nil
	}
}

func (p *Page) initToLine() {
	if p.toLine != nil {
		return
	}

	for i, b := range p.data {
		if b == '\n' {
			/*
				This means that the first entry in the array is the END of the first line. Read from 0:n for line 0, and so on.
				So line N you should read from p.data[p.toLine[N-1]:p.toLine[N]] if N >= 1 and p.data[0:p.toLine[0]] if N == 0
				Also this is the offset to the \n characters, so add 1 if you don't want to include that
			*/
			p.toLine = append(p.toLine, i)
		}
	}

	// Handle files without any newlines gracefully.
	if p.toLine == nil {
		p.toLine = []int{len(p.data) - 1}
	}
}

func (p *Page) IsRegular(path string) bool {
	fi, err := p.fs.Stat(path)
	return err == nil && fi.Mode().IsRegular()
}

func (p *Page) IsDir(path string) bool {
	fi, err := p.fs.Stat(path)
	return err == nil && fi.Mode().IsDir()
}

type systemFS struct{}

func (systemFS) Open(path string) (fs.File, error) {
	return os.Open(path) //nolint:wrapcheck
}

func (systemFS) Stat(name string) (fs.FileInfo, error) {
	return os.Stat(name) //nolint:wrapcheck
}
