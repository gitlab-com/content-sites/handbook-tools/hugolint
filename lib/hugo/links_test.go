package hugo

import (
	"errors"
	"fmt"
	"path"
	"testing"
	"testing/fstest"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestBrokenLinks(t *testing.T) {
	t.Parallel()

	const webRootDir = "fake"

	testFile := path.Join(webRootDir, "file_under_test.md")

	makePage := func(href string) string {
		md := fmt.Sprintf(`---
title: "Test"
---
Prefix [Link](%s) Suffix

### Heading
`, href)

		return md
	}

	makeLink := func(href string) []Link {
		return []Link{{
			Filename:   testFile,
			Href:       href,
			RelativeTo: webRootDir,
			Line:       4,
		}}
	}

	cases := []struct {
		name        string
		link        string
		fs          map[string]string
		withAnchors bool
		want        []Link
		wantErr     bool
	}{
		{
			name: "link to MD file",
			link: "/test/",
			fs:   touch(webRootDir + "/test.md"),
			want: nil,
		},
		{
			name: "link to directory with index.md",
			link: "/test/",
			fs:   touch(webRootDir + "/test/index.md"),
			want: nil,
		},
		{
			name: "link to directory with _index.md",
			link: "/test/",
			fs:   touch(webRootDir + "/test/_index.md"),
			want: nil,
		},
		{
			name: "link to directory with _index.html",
			link: "/test/",
			fs:   touch(webRootDir + "/test/_index.html"),
			want: nil,
		},
		{
			name: "trim .html suffix",
			link: "/test.html",
			fs:   touch(webRootDir + "/test/_index.md"),
			want: nil,
		},
		{
			name: "trim /index.html suffix",
			link: "/test/index.html",
			fs:   touch(webRootDir + "/test/_index.md"),
			want: nil,
		},
		{
			name: "broken link",
			link: "/test/",
			want: makeLink("/test/"),
		},
		{
			name:    "unparsable link",
			link:    "\a",
			wantErr: true,
		},
		{
			name: "external link",
			link: "https://example.com/",
			want: nil,
		},
		{
			name: "relative link to directory with index.md",
			link: "test/",
			fs:   touch(webRootDir + "/test/index.md"),
			want: nil,
		},
		{
			name: "link to .md file",
			link: "/test.md",
			fs:   touch(webRootDir + "/test.md"),
			want: nil,
		},
		{
			name: "relative link to .md file",
			link: "test.md",
			fs:   touch(webRootDir + "/test.md"),
			want: nil,
		},
		{
			name: "without WithAnchors, anchor links are ignored",
			link: "#does-not-exist",
			want: nil,
		},
		{
			name:        "link to heading",
			link:        "#heading",
			withAnchors: true,
			want:        nil,
		},
		{
			name:        "link to non-existing heading",
			link:        "#does-not-exist",
			withAnchors: true,
			want:        makeLink("#does-not-exist"),
		},
		{
			name: "link to external heading",
			link: "/external/#example",
			fs: map[string]string{
				webRootDir + "/external/index.md": "### Example Heading {#example}\n",
			},
			withAnchors: true,
			want:        nil,
		},
		{
			name: "link to external heading",
			link: "/external/#does-not-exist",
			fs: map[string]string{
				webRootDir + "/external/index.md": "### Example Heading {#example}\n",
			},
			withAnchors: true,
			want:        makeLink("/external/#does-not-exist"),
		},
		// https://gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/-/issues/3#note_2166898495
		{
			name: "gitlab_token",
			link: "#gitlab_token",
			fs: map[string]string{
				testFile: "" +
					"1. [`GITLAB_TOKEN`](#gitlab_token), currently called `Docsy GitLab Release`." +
					"\n" +
					"#### `$GITLAB_TOKEN`\n" +
					"\n" +
					"Tools or scripts that need to interact with the repositories ...\n",
			},
			withAnchors: true,
			want:        nil,
		},
		{
			name: "gitlab_token workaround",
			link: "#gitlab_token",
			fs: map[string]string{
				testFile: "" +
					"1. [`GITLAB_TOKEN`](#gitlab_token), currently called `Docsy GitLab Release`." +
					"\n" +
					"#### `$GITLAB_TOKEN` {#gitlab_token}\n" +
					"\n" +
					"Tools or scripts that need to interact with the repositories ...\n",
			},
			withAnchors: true,
			want:        nil,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			files := tc.fs
			if files == nil {
				files = make(map[string]string)
			}

			if _, ok := files[testFile]; !ok {
				files[testFile] = makePage(tc.link)
			}

			p, err := LoadPage(testFile, WithFS(newMapFS(files)))
			if err != nil {
				t.Fatalf("ReadPage() = %v", err)
			}

			got, err := p.BrokenLinks(webRootDir, WithAnchors(tc.withAnchors))
			if gotErr := err != nil; gotErr != tc.wantErr {
				t.Fatalf("BrokenLinks() = (%v, %v), want (%v, error:%v)", got, err, tc.want, tc.wantErr)
			}

			opts := []cmp.Option{
				cmpopts.IgnoreFields(Link{}, "Page"),
			}

			if diff := cmp.Diff(tc.want, got, opts...); diff != "" {
				t.Errorf("BrokenLinks() mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func touch(paths ...string) map[string]string {
	fs := make(map[string]string)

	for _, p := range paths {
		fs[p] = ""
	}

	return fs
}

func newMapFS(files map[string]string) fstest.MapFS {
	fs := make(fstest.MapFS)

	for name, data := range files {
		fs[name] = &fstest.MapFile{
			Data: []byte(data),
			Mode: 0644,
		}
	}

	return fs
}

func TestPage_Headings(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name string
		data string
		want []Heading
	}{
		{
			name: "autoheading with space",
			data: "## Heading with space\n",
			want: []Heading{{
				Slug: `heading-with-space`,
				Line: 1,
			}},
		},
		{
			name: "autoheading with umlaut",
			data: "## Überschrift\n",
			want: []Heading{{
				Slug: `überschrift`,
				Line: 1,
			}},
		},
		{
			name: "duplicate autoheading",
			data: "## Heading\n\n## Heading\n",
			want: []Heading{
				{
					Slug: `heading`,
					Line: 1,
				},
				{
					Slug: `heading-1`,
					Line: 3,
				},
			},
		},
		// https://gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/-/issues/3#note_2166898495
		{
			name: "with underscore",
			data: "## `$BUILD_AND_TEST_ONLY` mode\n",
			want: []Heading{{
				Slug: `build_and_test_only-mode`,
				Line: 1,
			}},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			page := &Page{
				path: "/test/foo",
				data: []byte(tc.data),
			}

			got, err := page.Headings()
			if err != nil {
				t.Fatal(err)
			}

			opts := []cmp.Option{
				cmpopts.IgnoreFields(Heading{}, "Filename"),
			}

			if diff := cmp.Diff(tc.want, got, opts...); diff != "" {
				t.Errorf("page.Headings() differs (-want/+got):\n%s", diff)
			}
		})
	}
}

//nolint:gocognit
func TestLink_Abs(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name       string
		path       string
		webRootDir string
		content    string
		want       string
		wantErr    error
	}{
		{
			name:       "abs link",
			path:       "content/_index.md",
			webRootDir: "content",
			content:    "[to](/foo/)",
			want:       "/foo/",
		},
		{
			name:       "absolute link with anchor",
			path:       "content/foo/_index.md",
			webRootDir: "content",
			content:    "[to](/bar/#baz)",
			want:       "/bar/#baz",
		},
		{
			name:       "relative link",
			path:       "content/foo/_index.md",
			webRootDir: "content",
			content:    "[to](bar/)",
			want:       "/foo/bar/",
		},
		{
			name:       "relative link with anchor",
			path:       "content/foo/_index.md",
			webRootDir: "content",
			content:    "[to](../bar/#baz)",
			want:       "/bar/#baz",
		},
		{
			name:       "anchor link",
			path:       "content/foo/_index.md",
			webRootDir: "content",
			content:    "[to](#heading)",
			want:       "/foo/#heading",
		},
		{
			name:       "external links throw an error",
			path:       "content/foo/_index.md",
			webRootDir: "content",
			content:    "[to](//gitlab.com/)",
			wantErr:    ErrExternal,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			fs := fstest.MapFS{
				tc.path: &fstest.MapFile{
					Data: []byte(tc.content),
				},
			}

			page, err := LoadPage(tc.path, WithFS(fs))
			if err != nil {
				t.Fatal(err)
			}

			links, err := page.Links()
			if err != nil {
				t.Fatal(err)
			}

			if len(links) != 1 {
				t.Fatalf("page.Links() = %v, want one link", links)
			}

			got, err := links[0].Abs(tc.webRootDir)
			if !errors.Is(err, tc.wantErr) {
				t.Fatalf("Abs(%q): got error %q, want error %q", tc.webRootDir, err, tc.wantErr)
			}

			if err != nil {
				return
			}

			if got != tc.want {
				t.Errorf("Abs(%q) = %q, want %q", tc.webRootDir, got, tc.want)
			}
		})
	}
}

func TestTrimTextFragment(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name     string
		fragment string
		want     string
	}{
		{
			name:     "empty string",
			fragment: "",
			want:     "",
		},
		{
			name:     "anchor link",
			fragment: "foo",
			want:     "foo",
		},
		{
			name:     "text fragment",
			fragment: ":~:text=Testing",
			want:     "",
		},
		{
			name:     "anchor with text fragment",
			fragment: "foo:~:text=Testing",
			want:     "foo",
		},
		{
			name:     "unicode anchor with text fragment",
			fragment: "grüße:~:text=Testing",
			want:     "grüße",
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			got := trimTextFragment(tc.fragment)
			if got != tc.want {
				t.Errorf("trimTextFragment(%q) = %q, want %q", tc.fragment, got, tc.want)
			}
		})
	}
}
