// Package config implements the Config structure, the `-config` flag, and configuration loading.
package config

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"regexp"

	"github.com/spf13/cobra"
	yaml "gopkg.in/yaml.v3"
)

const defaultConfigPath = ".hugolint.yaml"

var (
	ErrInvalidRegexp = errors.New("invalid regular expression")
	ErrInvalidYAML   = errors.New("invalid YAML syntax")
)

var configFlag string

// AddFlag adds the `--config` flag to the command's persistent flag set.
func AddFlag(cmd *cobra.Command) {
	cmd.PersistentFlags().StringVar(&configFlag, "config", "", "Name of the config file to read.")
}

// Config represents the hugolint configuration.
type Config struct {
	Excludes Excludes `yaml:"exclude,omitempty"`
}

// Load loads a config file based on the value of the command line flag.
func Load() (*Config, error) {
	if configFlag == "" {
		return LoadPath(defaultConfigPath, ignoreNotExist(true))
	}

	return LoadPath(configFlag)
}

// LoadPath loads a config file at the provided path.
func LoadPath(path string, opts ...Option) (*Config, error) {
	options := options{
		fs: systemFS{},
	}

	for _, o := range opts {
		o(&options)
	}

	fh, err := options.fs.Open(path)

	switch {
	case errors.Is(err, fs.ErrNotExist) && options.ignoreNotExist:
		return defaultConfig(), nil
	case err != nil:
		return nil, fmt.Errorf("Open(%q): %w", path, err)
	}

	defer fh.Close()

	dec := yaml.NewDecoder(fh)
	dec.KnownFields(true)

	var config Config
	if err := dec.Decode(&config); err != nil {
		return nil, fmt.Errorf("%w: %w", ErrInvalidYAML, err)
	}

	return &config, nil
}

func defaultConfig() *Config {
	return &Config{
		Excludes: nil,
	}
}

type options struct {
	fs             fs.FS
	ignoreNotExist bool
}

type Option func(o *options)

func WithFS(fs fs.FS) Option {
	return func(o *options) {
		o.fs = fs
	}
}

func ignoreNotExist(b bool) Option {
	return func(o *options) {
		o.ignoreNotExist = b
	}
}

// Excludes is a list of excludes.
//
//nolint:recvcheck // UnmarshalYAML required a pointer receiver.
type Excludes []Exclude

func (es *Excludes) UnmarshalYAML(node *yaml.Node) error {
	var raw []Exclude

	if err := node.Decode(&raw); err != nil {
		return fmt.Errorf("node.Decode(*[]Exclude): %w", err)
	}

	var errs []error

	for i := range raw {
		re, err := regexp.Compile(raw[i].Pattern)
		if err != nil {
			errs = append(errs, err)
		}

		if len(errs) != 0 {
			return fmt.Errorf("%w: %w", ErrInvalidRegexp, errors.Join(errs...))
		}

		raw[i].re = re
	}

	*es = Excludes(raw)

	return nil
}

// Match calls `Match` on each exclude in order and returns true if any of the excludes match.
func (es Excludes) Match(href string) bool {
	for _, e := range es {
		if e.Match(href) {
			return true
		}
	}

	return false
}

type Exclude struct {
	Pattern string `yaml:"pattern,omitempty"`

	re *regexp.Regexp
}

func (e *Exclude) Match(href string) bool {
	return e.re.MatchString(href)
}

// systemFS implements the fs.FS interface.
// This is different from the file system returned by os.DirFS() in the handling of relative paths.
type systemFS struct{}

func (systemFS) Open(name string) (fs.File, error) {
	return os.Open(name) //nolint:wrapcheck
}
