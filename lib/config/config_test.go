package config_test

import (
	"errors"
	"testing"
	"testing/fstest"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"

	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/config"
)

func TestLoadPath(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name    string
		data    []byte
		want    *config.Config
		wantErr error
	}{
		{
			name: "success",
			data: []byte("exclude:\n" +
				"  - pattern: '^/handbook/product/categories/($|#)'\n"),
			want: &config.Config{
				Excludes: []config.Exclude{
					{Pattern: `^/handbook/product/categories/($|#)`},
				},
			},
		},
		{
			name: "invalid regex",
			data: []byte("exclude:\n" +
				"  - pattern: '^/handbook/product/categories/('\n"),
			wantErr: config.ErrInvalidRegexp,
		},
		{
			name: "invalid yaml",
			data: []byte("exclude:\n" +
				"  - pattern: 'foo:"),
			wantErr: config.ErrInvalidYAML,
		},
		{
			name: "unknown field",
			data: []byte("exclude: []\n" +
				"invalid: true\n"),
			wantErr: config.ErrInvalidYAML,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			fs := fstest.MapFS{
				".hugolint.yaml": &fstest.MapFile{
					Data: tc.data,
				},
			}

			cfg, err := config.LoadPath(".hugolint.yaml", config.WithFS(fs))
			if !errors.Is(err, tc.wantErr) {
				t.Errorf("config.Load() = %v, want error %v", err, tc.wantErr)
			}

			if err != nil {
				return
			}

			opts := []cmp.Option{
				cmpopts.IgnoreFields(config.Exclude{}, "re"),
			}

			if diff := cmp.Diff(tc.want, cfg, opts...); diff != "" {
				t.Errorf("config.Load() differs (-want/+got):\n%s", diff)
			}
		})
	}
}

func TestConfig_ExcludesMatch(t *testing.T) {
	t.Parallel()

	fs := fstest.MapFS{
		".hugolint.yaml": &fstest.MapFile{
			Data: []byte(
				"exclude:\n" +
					"  - pattern: '^/handbook/product/categories/?($|#)'\n"),
		},
	}

	cfg, err := config.LoadPath(".hugolint.yaml", config.WithFS(fs))
	if err != nil {
		t.Fatal(err)
	}

	cases := []struct {
		href string
		want bool
	}{
		{"/handbook/product/categories", true},
		{"/handbook/product/categories/", true},
		{"/handbook/product/categories/#anchor", true},
		{"/handbook/product/categories/features", false},
		{"/handbook/product/categories/features/", false},
	}

	for _, tc := range cases {
		got := cfg.Excludes.Match(tc.href)
		if got != tc.want {
			t.Errorf("cfg.ExcludesMatch(%q) = %v, want %v", tc.href, got, tc.want)
		}
	}
}
