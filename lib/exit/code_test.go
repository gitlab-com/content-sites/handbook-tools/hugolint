package exit_test

import (
	"errors"
	"os"

	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/exit"
)

func ExampleCode() { //nolint:testableexamples
	// Example function that may return an error code.
	// This may be a sub-command that wants to exit with a specific error code, but does not wish an error to be printed.
	cmd := func(n int) error {
		if n == 0 {
			return exit.Code(1)
		}

		return nil
	}

	// Call the function and (possibly) retrieve an error.
	err := cmd(1)

	// Check if the error is an exit.ErrorCode.
	var code exit.Code

	switch {
	case errors.As(err, &code):
		// Exit with the error code.
		os.Exit(code.Int())
	case err != nil:
		// Handle other errors.
		os.Exit(1)
	}
}
