module gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint

go 1.24.0

require (
	github.com/fatih/color v1.18.0
	github.com/google/go-cmp v0.6.0
	github.com/octo/retry v1.0.2
	github.com/spf13/cobra v1.9.1
	github.com/yuin/goldmark v1.7.8
	gitlab.com/gitlab-org/api/client-go v0.123.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/rogpeppe/go-internal v1.11.0 // indirect
	golang.org/x/oauth2 v0.25.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	golang.org/x/time v0.9.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.6 // indirect
)
