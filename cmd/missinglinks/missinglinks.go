package missinglinks

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/hugo"
)

func Command() *cobra.Command {
	chk := &checker{}

	cmd := &cobra.Command{
		Use:   "missinglinks",
		Short: "Print broken links ordered by number of occurrences",
		Args:  cobra.MinimumNArgs(1),
		RunE:  chk.Run,
	}

	flagSet := cmd.Flags()
	flagSet.StringVar(&chk.webRoot, "webroot", "content", "Directory inside the repository that is the web root.")
	flagSet.StringVar(&chk.repoRoot, "reporoot", ".", "Directory that is the root of the repository.")
	flagSet.IntVar(&chk.num, "num", 0, "Number of links to print.")
	flagSet.BoolVar(&chk.checkAnchors, "check-anchors", false, "Check the anchor of links, not just the file existence.")

	return cmd
}

type checker struct {
	webRoot  string
	repoRoot string
	num      int

	checkAnchors bool
}

func (c *checker) Run(cmd *cobra.Command, args []string) error {
	cwd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("os.Getwd(): %w", err)
	}

	repoRootDir := c.repoRoot
	if !filepath.IsAbs(repoRootDir) {
		repoRootDir = filepath.Join(cwd, repoRootDir)
	}

	webRootDir := c.webRoot
	if !filepath.IsAbs(webRootDir) {
		webRootDir = filepath.Join(repoRootDir, webRootDir)
	}

	links := map[string]int{}

	for _, path := range args {
		if err := c.checkPath(path, webRootDir, links); err != nil {
			log.Print(err)
		}
	}

	fmt.Print(format(links, c.num))

	return nil
}

func (c *checker) checkPath(path, webRootDir string, links map[string]int) error {
	page, err := hugo.LoadPage(path)
	if err != nil {
		return fmt.Errorf("error loading page from %q: %w", path, err)
	}

	brokenLinks, err := page.BrokenLinks(webRootDir, hugo.WithAnchors(c.checkAnchors))
	if err != nil {
		return fmt.Errorf("error checking links for %q: %w", path, err)
	}

	for _, link := range brokenLinks {
		u, err := url.Parse(link.Href)
		if err != nil {
			continue
		}

		linkPath := u.Path
		if linkPath == "" {
			var err error

			linkPath, err = urlPath(webRootDir, path)
			if err != nil {
				return err
			}
		}

		displayName := linkPath
		if c.checkAnchors && u.Fragment != "" {
			displayName = linkPath + "#" + u.Fragment
		}

		if !strings.HasPrefix(displayName, "/") {
			continue
		}

		links[displayName]++
	}

	return nil
}

func urlPath(webRootDir, filePath string) (string, error) {
	absPath, err := filepath.Abs(filePath)
	if err != nil {
		return "", fmt.Errorf("filepath.Abs(%q): %w", filePath, err)
	}

	relPath, err := filepath.Rel(webRootDir, absPath)
	if err != nil {
		return "", fmt.Errorf("filepath.Rel(%q, %q): %w", webRootDir, absPath, err)
	}

	return "/" + filepath.ToSlash(relPath), nil
}

func format(links map[string]int, num int) string {
	if num == 0 || num > len(links) {
		num = len(links)
	}

	var linkCounts []linkCount

	for path, count := range links {
		linkCounts = append(linkCounts, linkCount{path, count})
	}

	sort.Sort(byCount(linkCounts))

	var b strings.Builder
	for i := range num {
		fmt.Fprintf(&b, "%-3d %s\n", linkCounts[i].Count, linkCounts[i].Path)
	}

	return b.String()
}

type linkCount struct {
	Path  string
	Count int
}

type byCount []linkCount

func (c byCount) Len() int      { return len(c) }
func (c byCount) Swap(i, j int) { c[i], c[j] = c[j], c[i] }
func (c byCount) Less(i, j int) bool {
	if c[i].Count == c[j].Count {
		return c[i].Path < c[j].Path
	}

	return c[i].Count > c[j].Count
}
