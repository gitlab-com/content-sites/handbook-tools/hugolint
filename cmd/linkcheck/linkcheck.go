package linkcheck

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/config"
	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/gitlab"
	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/hugo"
)

var ErrIssuesFound = errors.New("linkcheck found issues")

func Command() *cobra.Command {
	chk := &checker{}

	cmd := &cobra.Command{
		Use:   "linkcheck",
		Short: "Check for broken links",
		Args:  cobra.MinimumNArgs(1),
		RunE:  chk.Run,
	}

	flagSet := cmd.Flags()
	flagSet.StringVar(&chk.webRootFlag, "webroot", "content", "Directory inside the repository that is the web root.")
	flagSet.StringVar(&chk.repoRootFlag, "reporoot", ".", "Directory that is the root of the repository.")
	flagSet.StringVar(&chk.formatFlag, "format", "default", "Output format. One of (default, summary, json).")
	flagSet.BoolVar(&chk.checkAnchorsFlag, "check-anchors", true, "Check the anchor of links, not just the file existence.")

	return cmd
}

type checker struct {
	// flags
	webRootFlag      string
	repoRootFlag     string
	formatFlag       string
	checkAnchorsFlag bool

	// repoRoot is the filesystem used to access files.
	repoRoot    fs.StatFS
	repoRootDir string
	webRootDir  string
	excludes    matcher
}

func (c *checker) Run(cmd *cobra.Command, args []string) error {
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	c.repoRootDir = c.repoRootFlag
	if !filepath.IsAbs(c.repoRootDir) {
		c.repoRootDir = filepath.Join(cwd, c.repoRootDir)
	}

	var ok bool

	c.repoRoot, ok = os.DirFS(c.repoRootDir).(fs.StatFS)
	if !ok {
		//nolint:err113
		return fmt.Errorf("type %T returned by os.DirFS() does not implement fs.StatFS", os.DirFS(c.repoRootDir))
	}

	c.webRootDir = c.webRootFlag
	if filepath.IsAbs(c.webRootDir) {
		c.webRootDir, err = filepath.Rel(c.repoRootDir, c.webRootDir)
		if err != nil {
			return fmt.Errorf("filepath.Rel(%q, %q): %w", c.repoRootDir, c.webRootDir, err)
		}
	}

	c.excludes = matchNone{}

	cfg, err := config.Load()
	if err != nil {
		return fmt.Errorf("config.Load(): %w", err)
	}

	c.excludes = cfg.Excludes

	log.Printf("Repository root (used for quality report): %s\n", c.repoRootDir)
	log.Printf("Web root (used for relative links):        %s\n", c.webRootDir)

	findings, err := c.checkAll(args)
	if err != nil {
		return err
	}

	formatter, err := c.formatter()
	if err != nil {
		return err
	}

	output, err := formatter.Format(findings)
	if err != nil {
		return fmt.Errorf("formatter.Format(): %w", err)
	}

	fmt.Println(output)

	return nil
}

func (c *checker) checkAll(args []string) ([]gitlab.CodeQuality, error) {
	var findings []gitlab.CodeQuality

	for _, path := range args {
		f, err := c.checkPath(path)

		var ae hugo.AnchoredError
		if errors.As(err, &ae) {
			findings = append(findings, newFinding(gitlab.Critical, ae.Link, c.repoRootDir, ae.Err.Error()))
			continue
		}

		if err != nil {
			return nil, fmt.Errorf("checkPath(%q): %w", path, err)
		}

		findings = append(findings, f...)
	}

	return findings, nil
}

//nolint:ireturn
func (c *checker) formatter() (formatter, error) {
	switch c.formatFlag {
	case "default":
		return &defaultFormatter{}, nil
	case "summary":
		return &summaryFormatter{}, nil
	case "json":
		return &jsonFormatter{}, nil
	default:
		return nil, fmt.Errorf("unknown output format %q", c.formatFlag) //nolint:err113
	}
}

func (c *checker) checkPath(path string) ([]gitlab.CodeQuality, error) {
	page, err := hugo.LoadPage(path, hugo.WithFS(c.repoRoot))
	if err != nil {
		return nil, fmt.Errorf("error loading page from %q: %w", path, err)
	}

	brokenLinks, err := page.BrokenLinks(c.webRootDir, hugo.WithAnchors(c.checkAnchorsFlag))
	if err != nil {
		return nil, fmt.Errorf("error checking links for %q: %w", path, err)
	}

	var findings []gitlab.CodeQuality

	for _, link := range brokenLinks {
		if c.excludes.Match(link.Href) {
			continue
		}

		msg := fmt.Sprintf("Link destination %q does not exist", link.Href)
		findings = append(findings, newFinding(gitlab.Major, link, c.repoRootDir, msg))
	}

	return findings, nil
}

func newFinding(severity gitlab.Severity, link hugo.Link, repoRootDir, msg string) gitlab.CodeQuality {
	relPath, err := filepath.Rel(repoRootDir, link.Filename)
	if err != nil {
		relPath = link.Filename
	}

	return gitlab.CodeQuality{
		Description: msg,
		Name:        "HugoLinkCheck",
		Severity:    severity,
		Path:        relPath,
		Line:        link.Line,
	}
}

type formatter interface {
	Format(findings []gitlab.CodeQuality) (string, error)
	Error(findings []gitlab.CodeQuality) error
}

type defaultFormatter struct{}

func (defaultFormatter) Format(findings []gitlab.CodeQuality) (string, error) {
	var b strings.Builder

	var prevFile string
	for _, f := range findings {
		if prevFile != f.Path {
			fmt.Fprintln(&b, f.Path)
			prevFile = f.Path
		}

		fmt.Fprintf(&b, "* %s:%d: %s\n", f.Path, f.Line, f.Description)
	}

	return b.String(), nil
}

func (defaultFormatter) Error(findings []gitlab.CodeQuality) error {
	if len(findings) > 0 {
		return ErrIssuesFound
	}

	return nil
}

type summaryFormatter struct{}

func (summaryFormatter) Format(findings []gitlab.CodeQuality) (string, error) {
	var (
		brokenLinks   int
		relativeLinks int
	)

	for _, f := range findings {
		switch f.Severity { //nolint:exhaustive
		case gitlab.Major:
			brokenLinks++
		case gitlab.Minor:
			relativeLinks++
		}
	}

	switch {
	case brokenLinks != 0 && relativeLinks != 0:
		return fmt.Sprintf("❌ Found %d broken links and %d relative links", brokenLinks, relativeLinks), nil
	case relativeLinks != 0:
		return fmt.Sprintf("⚠ Found %d relative links", relativeLinks), nil
	case relativeLinks == 0:
		return fmt.Sprintf("❌ Found %d broken links", brokenLinks), nil
	}

	return "✅ All links are valid", nil
}

func (summaryFormatter) Error(findings []gitlab.CodeQuality) error {
	if len(findings) > 0 {
		return ErrIssuesFound
	}

	return nil
}

type jsonFormatter struct{}

func (jsonFormatter) Format(findings []gitlab.CodeQuality) (string, error) {
	var b strings.Builder

	if err := json.NewEncoder(&b).Encode(findings); err != nil {
		return "", fmt.Errorf("json.NewEncoder().Encode(): %w", err)
	}

	return b.String(), nil
}

func (jsonFormatter) Error(_ []gitlab.CodeQuality) error {
	return nil
}

type matcher interface {
	Match(href string) bool
}

type matchNone struct{}

func (matchNone) Match(_ string) bool {
	return false
}
