package linkcheck

import (
	"bytes"
	"fmt"
	"iter"
	"maps"
	"testing"
	"testing/fstest"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"

	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/config"
	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/gitlab"
)

func TestCheckAll(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name    string
		fs      fstest.MapFS
		exclude matcher
		want    []gitlab.CodeQuality
	}{
		{
			name: "page found",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/)"),
				},
				"content/dest/_index.md": &fstest.MapFile{
					Data: []byte("exists"),
				},
			},
			want: nil,
		},
		{
			name: "page not found",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/)"),
				},
			},
			want: []gitlab.CodeQuality{{
				Path: "content/src/_index.md",
				Line: 1,
			}},
		},
		{
			name: "anchor found",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/#heading)"),
				},
				"content/dest/_index.md": &fstest.MapFile{
					Data: []byte("# Heading"),
				},
			},
			want: nil,
		},
		{
			name: "anchor not found",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/#heading)"),
				},
				"content/dest/_index.md": &fstest.MapFile{
					Data: []byte("# Other"),
				},
			},
			want: []gitlab.CodeQuality{{
				Path: "content/src/_index.md",
				Line: 1,
			}},
		},
		{
			name: "text fragment",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/#:~:text=Heading)"),
				},
				"content/dest/_index.md": &fstest.MapFile{
					Data: []byte("# Heading"),
				},
			},
			want: nil,
		},
		{
			name: "text fragment with anchor",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/#heading:~:text=Heading)"),
				},
				"content/dest/_index.md": &fstest.MapFile{
					Data: []byte("# Heading"),
				},
			},
			want: nil,
		},
		{
			name: "text fragment with broken anchor",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/#broken:~:text=Heading)"),
				},
				"content/dest/_index.md": &fstest.MapFile{
					Data: []byte("# Heading"),
				},
			},
			want: []gitlab.CodeQuality{{
				Path: "content/src/_index.md",
				Line: 1,
			}},
		},
		{
			name: "excluded link not found",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/)"),
				},
			},
			exclude: pattern("^/dest/?$"),
			want:    nil,
		},
		{
			name: "anchor in excluded page not found",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/dest/#heading)"),
				},
				"content/dest/_index.md": &fstest.MapFile{
					Data: []byte("# Other"),
				},
			},
			exclude: pattern("^/dest/#"),
			want:    nil,
		},
		{
			name: "link to team member",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/company/team/#member)"),
				},
				"content/company/team.md": &fstest.MapFile{
					Data: []byte("{{< team >}}"),
				},
			},
			want: []gitlab.CodeQuality{{
				Path: "content/src/_index.md",
				Line: 1,
			}},
		},
		{
			name: "link to team member: ignore anchor links",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/company/team/#member)"),
				},
				"content/company/team.md": &fstest.MapFile{
					Data: []byte("{{< team >}}"),
				},
			},
			exclude: pattern("^/company/team/?($|#)"),
			want:    nil,
		},
		{
			name: "link to team member: ignore page links",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte(
						"[ignored](/company/team/#member)\n\n" + // broken, ignored
							"[reported](/company/values/#greed)\n"), // broken, reported
				},
				"content/company/team.md": &fstest.MapFile{
					Data: []byte("{{< team >}}"),
				},
				"content/company/values.md": &fstest.MapFile{
					Data: []byte("# Transparency"),
				},
			},
			exclude: pattern("^/company/team/?($|#)"),
			want: []gitlab.CodeQuality{{
				Path: "content/src/_index.md",
				Line: 3,
			}},
		},
		{
			name: "link to team member: ignore directory",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[dest](/company/team/#member)"),
				},
				"content/company/team.md": &fstest.MapFile{
					Data: []byte("{{< team >}}"),
				},
			},
			exclude: pattern("^/company/"),
			want:    nil,
		},
		{
			name: "subpages are not excluded",
			fs: fstest.MapFS{
				"content/src/_index.md": &fstest.MapFile{
					Data: []byte("[ignored](/handbook/product/categories/#broken\n\n" +
						"[reported](/handbook/product/categories/features/#broken)\n"),
				},
				"content/handbook/product/categories/_index.md": &fstest.MapFile{
					Data: []byte("{{% categories %}}"),
				},
				"content/handbook/product/categories/features/_index.md": &fstest.MapFile{
					Data: []byte("# Features"),
				},
			},
			// exclude "/handbook/product/categories/" but not "/handbook/product/categories/features/".
			exclude: pattern("^/handbook/product/categories/?($|#)"),
			want: []gitlab.CodeQuality{{
				Path: "content/src/_index.md",
				Line: 3,
			}},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			chk := &checker{
				checkAnchorsFlag: true,
				repoRootDir:      ".",
				repoRoot:         tc.fs,
				webRootDir:       "content",
				excludes:         matchNone{},
			}

			if tc.exclude != nil {
				chk.excludes = tc.exclude
			}

			got, err := chk.checkAll(slice(maps.Keys(tc.fs)))
			if err != nil {
				t.Fatal(err)
			}

			opts := []cmp.Option{
				cmpopts.IgnoreFields(gitlab.CodeQuality{}, "Description", "Name", "Severity"),
			}

			if diff := cmp.Diff(tc.want, got, opts...); diff != "" {
				t.Errorf("checkAll: result differs (-want/+got):\n%s", diff)
			}
		})
	}
}

func slice(iter iter.Seq[string]) []string {
	var ret []string

	iter(func(s string) bool {
		ret = append(ret, s)
		return true
	})

	return ret
}

func pattern(ps ...string) config.Excludes {
	var b bytes.Buffer

	fmt.Fprintln(&b, "exclude:")

	for _, p := range ps {
		fmt.Fprintf(&b, "  - pattern: %q\n", p)
	}

	fs := fstest.MapFS{
		".hugolint.yaml": &fstest.MapFile{
			Data: b.Bytes(),
		},
	}

	cfg, err := config.LoadPath(".hugolint.yaml", config.WithFS(fs))
	if err != nil {
		panic(err)
	}

	return cfg.Excludes
}
