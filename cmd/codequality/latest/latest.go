//nolint:err113
package latest

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"slices"
	"time"

	"github.com/octo/retry"
	"github.com/spf13/cobra"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

type implementation struct {
	projectID    string
	jobName      string
	targetBranch string
	artifactName string
	commitSHA    string
}

func Command() *cobra.Command {
	impl := &implementation{}

	cmd := &cobra.Command{
		Use:   "latest",
		Short: "Fetch the latest code quality report from a commit, typically the merge base.",
		Args:  cobra.ExactArgs(0),
		RunE:  impl.run,
	}

	flags := cmd.Flags()
	flags.StringVar(&impl.projectID, "project", os.Getenv("CI_PROJECT_ID"), "GitLab project ID")
	flags.StringVar(&impl.jobName, "job", os.Getenv("CI_JOB_NAME"), "GitLab pipeline job name")
	flags.StringVar(&impl.targetBranch, "target-branch", "", "Name of the target branch")
	flags.StringVar(&impl.commitSHA, "commit", "", "SHA of the commit at which to download the report")
	flags.StringVar(&impl.artifactName, "artifact", "", "Name of the artifact (required)")

	return cmd
}

func (impl *implementation) run(cmd *cobra.Command, _ []string) error {
	if impl.artifactName == "" {
		return errors.New(`the "--artifact" flag is required`)
	}

	client, err := newClient()
	if err != nil {
		return fmt.Errorf("gitlab.NewClient(<token>): %w", err)
	}

	if impl.commitSHA == "" && impl.targetBranch == "" {
		impl.commitSHA = defaultCommitSHA()
	}

	var reader io.Reader

	switch {
	case impl.commitSHA != "":
		reader, err = downloadSingleArtifactsFileBySHA(cmd.Context(), client, impl.projectID, impl.commitSHA, impl.jobName, impl.artifactName)
		if err != nil {
			return fmt.Errorf("downloadSingleArtifactsFileBySHA(project=%q, commit=%q, job=%q, path=%q): %w",
				impl.projectID, impl.commitSHA, impl.jobName, impl.artifactName, err)
		}

	case impl.targetBranch != "":
		reader, _, err = client.Jobs.DownloadSingleArtifactsFileByTagOrBranch(
			impl.projectID, impl.targetBranch, impl.artifactName,
			&gitlab.DownloadArtifactsFileOptions{
				Job: gitlab.Ptr(impl.jobName),
			},
		)
		if err != nil {
			return fmt.Errorf("DownloadSingleArtifactsFileByTagOrBranch(project=%q, branch=%q, job=%q, path=%q): %w",
				impl.projectID, impl.targetBranch, impl.jobName, impl.artifactName, err)
		}

	default:
		return errors.New(`either the "--commit" or the "--merge-branch" flag is required`)
	}

	if _, err := io.Copy(os.Stdout, reader); err != nil {
		return fmt.Errorf("io.Copy(os.Stdout, reader): %w", err)
	}

	return nil
}

func downloadSingleArtifactsFileBySHA(ctx context.Context, client *gitlab.Client, projectID, commitSHA, jobName, artifactName string) (*bytes.Reader, error) {
	jobs, err := jobsForSHA(ctx, client, projectID, commitSHA, jobName)
	if err != nil {
		return nil, err
	}

	var lastErr error

	for _, job := range jobs {
		log.Printf("Fetching %q from %s\n", artifactName, job.WebURL)

		reader, _, err := client.Jobs.DownloadSingleArtifactsFile(projectID, job.ID, artifactName, gitlab.WithContext(ctx))
		if err != nil {
			log.Printf("Fetching %q from %s failed: %v\n", artifactName, job.WebURL, err)
			lastErr = err

			continue
		}

		return reader, nil
	}

	return nil, fmt.Errorf("downloading %q from %d jobs failed; last error: %w", artifactName, len(jobs), lastErr)
}

// jobsForSHA returns a list of all 'jobName' jobs for a given commit SHA.
// The returned list is sorted by the jobs' "finished at" time in descending order, i.e. the most recent job comes first.
func jobsForSHA(ctx context.Context, client *gitlab.Client, projectID, commitSHA, jobName string) ([]*gitlab.Job, error) {
	pipelines, err := pipelinesForSHA(ctx, client, projectID, commitSHA)
	if err != nil {
		return nil, err
	}

	var jobs []*gitlab.Job

	opts := []retry.Option{
		retry.Attempts(10),
		retry.ExpBackoff{
			Base:   10 * time.Second,
			Max:    1 * time.Minute,
			Factor: 2.0,
		},
		retry.FullJitter,
	}

	err = retry.Do(ctx, func(ctx context.Context) error {
		var (
			retryable bool
			err       error //nolint:govet // shadowing of err is intentional
		)

		jobs, retryable, err = jobsForPipelines(ctx, client, projectID, pipelines, jobName)
		if err != nil {
			return retry.Abort(err)
		}

		if len(jobs) != 0 {
			return nil
		}

		err = fmt.Errorf("no %q job found for commit %q", jobName, commitSHA)
		if !retryable {
			return retry.Abort(err)
		}

		return err
	}, opts...)

	if err != nil {
		return nil, err //nolint:wrapcheck
	}

	slices.SortFunc(jobs, func(p, q *gitlab.Job) int {
		switch {
		case p.FinishedAt == nil && q.FinishedAt == nil:
			return 0
		case p.FinishedAt == nil:
			return 1
		case q.FinishedAt == nil:
			return -1
		default:
			return q.FinishedAt.Compare(*p.FinishedAt)
		}
	})

	return jobs, nil
}

func pipelinesForSHA(ctx context.Context, client *gitlab.Client, projectID, commitSHA string) ([]*gitlab.PipelineInfo, error) {
	opts := gitlab.ListProjectPipelinesOptions{
		SHA: gitlab.Ptr(commitSHA),
	}

	var pipelines []*gitlab.PipelineInfo

	for {
		ps, resp, err := client.Pipelines.ListProjectPipelines(projectID, &opts, gitlab.WithContext(ctx))
		if err != nil {
			return nil, fmt.Errorf("ListProjectPipelines(project=%q, sha=%q): %w", projectID, commitSHA, err)
		}

		pipelines = append(pipelines, ps...)

		if resp.NextPage == 0 {
			break
		}

		opts.Page = resp.NextPage
	}

	if len(pipelines) == 0 {
		return nil, fmt.Errorf("no pipelines found for commit %q", commitSHA)
	}

	return pipelines, nil
}

func jobsForPipelines(ctx context.Context, client *gitlab.Client, projectID string, pipelines []*gitlab.PipelineInfo,
	jobName string) (jobs []*gitlab.Job, retryable bool, err error) {
	for _, pipeline := range pipelines {
		js, r, err := jobsForPipeline(ctx, client, projectID, pipeline.ID, jobName)
		if err != nil {
			return nil, false, err
		}

		jobs = append(jobs, js...)
		retryable = retryable || r

		// Return as soon as we have any job(s). All the jobs run on the same
		// commit, so they should all produce the same outcome, meaning we don't
		// care much which job's artifact we read, they should all be identical.
		if len(jobs) != 0 {
			return jobs, false, nil
		}
	}

	return jobs, retryable, nil
}

func jobsForPipeline(ctx context.Context, client *gitlab.Client, projectID string, pipelineID int,
	jobName string) (jobs []*gitlab.Job, retryable bool, err error) {
	opts := gitlab.ListJobsOptions{
		// Both successful and failed jobs can publish artifacts.
		// Ignore other status codes (pending, cancelled, …)
		Scope: gitlab.Ptr([]gitlab.BuildStateValue{
			gitlab.Success,
			gitlab.Failed,
			// Any of the following will result in retryOnEmpty=true
			gitlab.Created,
			gitlab.Pending,
			gitlab.Preparing,
			gitlab.Running,
			gitlab.Scheduled,
			gitlab.WaitingForResource,
		}),
	}

	for {
		js, resp, err := client.Jobs.ListPipelineJobs(projectID, pipelineID, &opts, gitlab.WithContext(ctx))
		if err != nil {
			return nil, false, fmt.Errorf("ListProjectPipelines(project=%q, pipeline_id=%d): %w", projectID, pipelineID, err)
		}

		for _, j := range js {
			if j.Name != jobName {
				continue
			}

			//nolint:exhaustive
			switch gitlab.BuildStateValue(j.Status) {
			case gitlab.Success, gitlab.Failed:
				jobs = append(jobs, j)
			case gitlab.Created, gitlab.Pending, gitlab.Preparing, gitlab.Running, gitlab.Scheduled, gitlab.WaitingForResource:
				retryable = true
			default:
				log.Printf("unhandled job state: job = %s, job.Status = %q", j.WebURL, j.Status)
			}
		}

		if resp.NextPage == 0 {
			break
		}

		opts.Page = resp.NextPage
	}

	return jobs, retryable, nil
}

//nolint:wrapcheck
func newClient() (*gitlab.Client, error) {
	if token := os.Getenv("CI_JOB_TOKEN"); token != "" {
		return gitlab.NewJobClient(token)
	}

	if token := os.Getenv("GITLAB_TOKEN"); token != "" {
		return gitlab.NewClient(token)
	}

	return nil, errors.New(`could not determine GitLab access token; set the "GITLAB_TOKEN" environment variable`)
}

func defaultCommitSHA() string {
	// Merged results pipelines set CI_MERGE_REQUEST_TARGET_BRANCH_SHA to the
	// HEAD of the target branch at the time the pipeline was started.
	if sha := os.Getenv("CI_MERGE_REQUEST_TARGET_BRANCH_SHA"); sha != "" {
		return sha
	}

	// For normal ("detached") merge request pipelines, use the merge base for
	// comparison.
	// This is also used when merged results pipelines are enabled but there is
	// a merge conflict.
	return os.Getenv("CI_MERGE_REQUEST_DIFF_BASE_SHA")
}
