package codequality

import (
	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/cmd/codequality/diff"
	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/cmd/codequality/latest"
)

func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "codequality <subcommand>",
		Short: "Various code quality utilities",
	}

	cmd.AddCommand(diff.Command())
	cmd.AddCommand(latest.Command())

	return cmd
}
