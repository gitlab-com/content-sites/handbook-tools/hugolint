package main

import (
	"context"
	"errors"
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/cmd/codequality"
	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/cmd/linkcheck"
	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/cmd/missinglinks"
	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/config"
	"gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint/lib/exit"
)

func main() {
	ctx := context.Background()

	cmdRoot := &cobra.Command{
		Use:          "hugolint",
		Short:        "Linter for the Hugo static site generator",
		SilenceUsage: true,
	}

	cmdRoot.AddCommand(codequality.Command())
	cmdRoot.AddCommand(linkcheck.Command())
	cmdRoot.AddCommand(missinglinks.Command())

	config.AddFlag(cmdRoot)

	err := cmdRoot.ExecuteContext(ctx)

	var code exit.Code

	switch {
	case errors.As(err, &code):
		os.Exit(code.Int())
	case err != nil:
		log.Print(err)
		os.Exit(1)
	}
}
