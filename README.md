# hugolint

Hugolint implements static analysis of Markdown files specific to Hugo.

## Usage

```
hugolint linkcheck [flags] <file ...>
hugolint codequality diff <file> <file>
```

## Code quality report

`hugolint linkcheck` generates a [Code Quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html) report when run with the `--format=json` flag.

If you upload the JSON output to GitLab, it will display a report of new and fixed issues in the Merge Request view. You can do so from the CI pipeline using the following configuration:

```yml
hugolint:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  image:
    name: registry.gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint:latest
  script:
    - hugolint linkcheck --format=json $(find content -type f -name '*.md') >hugolint-linkcheck.json
  artifacts:
    when: always
    expire_in: 1 month
    reports:
      codequality: hugolint-linkcheck.json
```

## Configuration

hugolint looks for a configuration file named `.hugolint.yaml` in the working directory. Alternatively you can specify a config file using the `--config` flag.

### Excluding false positives

False positive findings can be ignored using the `exclude` key.

Hugo uses a powerful templating system and *hugolint* will not be able to handle all cases correctly.
This can result in false positive findings, lowering the signal to noise ratio.

Each item provides a pattern, which is an [RE2 regular expression](https://github.com/google/re2/wiki/Syntax).
hugolint matches *links* against the regular expression and will ignore all matching links.

### Example 1: ignore anchor links

Let's say there is a page at `content/company/team.md` that uses Hugo's "shortcodes" to generate a bunch of headings.
*hugolint* will be unable to check anchor links to this page, but the feature should be otherwise enabled.

The following pattern can be used to ignore anchor links to this page:

```yaml
exclude:
  - pattern: '^/handbook/company/team/?#'
```

Notes:

* The regular expression is explicit *anchored* at the beginning using `^`. Without `^` the pattern would match anywhere in the string.
* The trailing slash is optional using `/?`.
* The `#` character is required, in other words non-anchor links are not excluded and will be checked.
* The anchor itself, i.e. anything following the `#` character, is ignored when matching.
* The regular expression is applied to the link. References like `../team/#user` will not be caught.
* The match is case sensitive.

### Example 2: ignore all links to a page but not any sub-pages.

If hugolint is unable to find the page entirely, not just anchors within the page, you may want to ignore all links to the page.
Since patterns are not anchored by default, care must be taken to craft the regular expression so it does not inadvertently ignore sub-pages.

The following pattern ignores all links to the "team" page but not any sub-pages:

```yaml
exclude:
  - pattern: '^/handbook/company/team/?(#|$)'
```

Notes:

* The pattern differs from _Example 1_ in that `team/` is either followed by an anchor or the end of string (`$`).
* The _Example 1_ notes also apply here.

### Example 3: ignore all links to a page including subpages

```yaml
exclude:
  - pattern: '^/handbook/company/team/'
```

Notes:

* The pattern matches the the path: `/handbook/company/team/…`, where `…` can be any sequence of characters, including the empty string.
* The _Example 1_ notes also apply here.

## Contributing and Releases

Hugolint uses [semantic release](https://github.com/semantic-release/semantic-release) to automatically create releases when there are new commits to the main branch.

When creating commits, or merge request titles, please use the outlined commit message convention.
